var express = require("express");
var router = express.Router();

// GET home page.
router.get("/", function (req, res) {
	console.log("C'était le 26 mai 93");
	res.render("index", {
		buteurName: `Buteur : ${process.env.SCORER_NAME}`,
		buteurB64Image: process.env.SCORER_IMAGE,
	});
});

module.exports = router;
