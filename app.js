var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var indexRouter = require("./routes/index");

var app = express();
const port = process.env.PORT || 3000;

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	//res.render("error");
});

//Server running
const server = app.listen(port, () => {
	console.log(`Server is running on port : ${port}`);
});

//Graceful shutdown
function handleShutdownGracefully(signal) {
	console.info(`${signal} signal received`);
	console.log("Closing HTTP server");
	server.close(() => {
		console.log("HTTP server closed");
		process.exit(0);
	});
}

process.on("SIGINT", () => handleShutdownGracefully("SIGINT"));
process.on("SIGTERM", () => handleShutdownGracefully("SIGTERM"));
process.on("SIGHUP", () => handleShutdownGracefully("SIGHUP"));
