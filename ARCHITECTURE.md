# TP K8S - "C'était le 26 mai 93" - Proposition 1

## Application

L'application est une application [Node.js](https://nodejs.org/fr/) exposant une API web grâce à [Express](https://expressjs.com/fr/) une infrastructure d'applications Web. Elle permet d'afficher "C'était le 26 mai 93" en page web et sur la sortie standard.

## Intégration continue

L'intégration continue consiste à pousser de petits morceaux de code vers la base de code principale de l'application hébergée dans un dépôt Git. À chaque `push`, une pipeline de scripts est exécutée pour construire, tester et valider les modifications du code avant de les fusionner dans la branche principale.

[GitLab](https://gitlab.com/) propose un outil intégré à leur solution pour le développement permettant d'implémenter cette intégration. Cet outil est plus connu sous le nom de [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

### Build de l'image docker

Utiliser du DinD (Docker in Docker) pour builder des images n'est pas vraiment idéal car il entraîne des problèmes de performances ainsi que des problèmes de sécurité.

Plusieurs solutions alternatives existent comme Buildah, Kaniko ou même l'utilisation de la Socket Docker pour builder dans un runner.

Pour ce TP, j'ai choisi d'utiliser [Buildah](https://buildah.io/).

### Stage de build de l'image Docker

```yml
build:
  stage: "build"
  image:
    name: "quay.io/buildah/stable:v1.21.0"
  script:
    - "buildah bud -f Dockerfile -t ${CI_REGISTRY_IMAGE}:${TAG} ."
    - "mkdir ${ARTIFACT_DIR}"
    - "buildah push ${CI_REGISTRY_IMAGE}:${TAG} docker-archive:${ARTIFACT_DIR}/${ARTIFACT_NAME}:${TAG}"
  artifacts:
    name: "archive:${ARTIFACT_NAME}:${CI_JOB_ID}"
    when: "on_success"
    expire_in: "6h"
    paths:
      - "${ARTIFACT_DIR}/"
```

Dans ce stage, on build avec buildah l'image docker de l'application grâce au Dockerfile qui lui a été fourni :

```Dockerfile
FROM docker.io/node:alpine

WORKDIR /app

COPY package*.json /app

RUN npm install

COPY . /app

CMD ["node", "./app/index.js"]
```

Ce dockerfile conteunerise simplement une application [Node.js](https://nodejs.org/fr/).

Ensuite, l'image buildée est sauvegardée dans une archive Docker et dans un job artifact.

### Stage de push de l'image sur Docker Hub

```yml
push:
  stage: "push"
  image:
    name: "quay.io/buildah/stable:v1.21.4"
  only:
    - "tags"
    - "main"
  script:
    - "buildah pull docker-archive:${ARTIFACT_DIR}/${ARTIFACT_NAME}"
    - "echo $CI_REGISTRY_PASSWORD | buildah login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY_NAME"
    - "buildah push ${CI_REGISTRY_IMAGE}:${TAG}"
  dependencies:
    - "build"
```

Dans ce stage, on récupère d'abord l'image docker qui a été build avec `buildah`, on se connecte au registry de Docker Hub puis enfin on push l'image buildée sur le registry spécifié : [drimox/tp-k8s](https://hub.docker.com/repository/docker/drimox/tp-k8s).

### Stage de déploiement

```yaml
deploy:
  stage: deploy
  image: dtzar/helm-kubectl
  script:
    - kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.48.1/deploy/static/provider/do/deploy.yaml
    - kubectl apply -f k8s
  dependencies:
    - "push"
```

Dans ce stage, on déploie l'application dans kubernetes grâce aux fichiers inclus dans le dossier `kubernetes`.

## Kubernetes

Pour information, pour ce TP j'utilise la solution gratuite fournie par [DigitalOcean](https://www.digitalocean.com/products/kubernetes/).

### Prérequis

Avant de configurer l'application clusterisée, il faut avoir au prélable installer les deux outils suivants :

- [kubectl](https://kubernetes.io/docs/tasks/tools/) : la cli officielle de Kubernetes
- [doctl](https://github.com/digitalocean/doctl) : la cli de DigitalOcean

### Mise en place

Installation des outils et connexion à DigitalOcean :

```bash
sudo snap install doctl
sudo snap connect doctl:kube-config
doctl auth init
```

Connexion au cluster :

```bash
cd ~/.kube && kubectl --kubeconfig="cluster-tp-k8s-kubeconfig.yaml" get nodes
NAME                STATUS   ROLES    AGE   VERSION
pool-tp-k8s-8krsd   Ready    <none>   22d   v1.21.2
pool-tp-k8s-8krsv   Ready    <none>   22d   v1.21.2
```

Création d'un namespace :

```bash
kubectl --kubeconfig="cluster-tp-k8s-kubeconfig.yaml" create namespace tp-k8s
kubectl --kubeconfig="cluster-tp-k8s-kubeconfig.yaml" config set-context --current --namespace=tp-k8s
```

Ingress controller :

```bash
kubectl --kubeconfig="cluster-tp-k8s-kubeconfig.yaml" apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.48.1/deploy/static/provider/do/deploy.yaml
```

Fichiers kubernetes pour l'application situés dans `kubernetes/` :

```bash
kubectl --kubeconfig="cluster-tp-k8s-kubeconfig.yaml" apply -f ~/Documents/DO/k8s/tp-k8s/kubernetes
```

Ajout de l'IP retournée par l'Ingress dans le fichier `/etc/hosts` :

```bash
46.101.68.104 do.exam.local
```
